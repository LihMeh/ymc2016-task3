package com.myarchway.mikhail.sign;


import android.content.ContentUris;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PictureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PictureFragment extends Fragment {
    private static final String IMAGE_ID = "image_id";

    private long imageId;

    private Bitmap currentImage;

    public PictureFragment() {
    }


    public static PictureFragment newInstance(long imageId) {
        PictureFragment fragment = new PictureFragment();
        Bundle args = new Bundle();
        args.putLong(IMAGE_ID, imageId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            imageId = getArguments().getLong(IMAGE_ID);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.test_menu,menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_picture, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        (new LoadThumbnailTask()).execute(imageId);
    }

    @Override
    public void onPause() {
        super.onPause();
        showImage(null);
    }

    private void showImage(Bitmap newImage) {
        if (getView()==null) {
            if (newImage!=null) newImage.recycle();
            return;
        }
        if (newImage!=null) {
            ((ImageView) getView().findViewById(R.id.imageView)).setImageBitmap(newImage);
        }
        else {
            ((ImageView) getView().findViewById(R.id.imageView)).setImageDrawable(null);
        }
        if ((currentImage!=null)&&(currentImage!=newImage)) {
            currentImage.recycle();
        }
        currentImage = newImage;
    }

    private class LoadThumbnailTask extends AsyncTask<Long,Object,Bitmap> {

        @Override
        protected Bitmap doInBackground(Long... imageId) {
            try {
                return MediaStore.Images.Thumbnails.getThumbnail(getContext().getContentResolver(),imageId[0],MediaStore.Images.Thumbnails.MINI_KIND, null);
            }catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            showImage(bitmap);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != R.id.procBtn) return true;
        (new ImageProcessorTask()).execute(imageId);
        return false;
    }

    private class ImageProcessorTask extends AsyncTask<Long,Object,Bitmap> {



        @Override
        protected Bitmap doInBackground(Long... params) {
            Bitmap originalImage = null;
            try {
                originalImage = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,params[0]));
                ImageProcessor processor = new ImageProcessor(getResources());
                //return processor.test(originalImage);
                return processor.findSigns(originalImage);
            } catch (Exception ex) {
                Log.e("image detector",ex.toString());
                return null;
            }
            finally {
                if (originalImage!=null) {
                    originalImage.recycle();
                }
            }
        }

        @Override
        protected void onPreExecute() {
            //Toast.makeText(getContext(),"started",Toast.LENGTH_SHORT).show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Bitmap processedImage) {
            if (processedImage==null) {
                Toast.makeText(getContext(),"failed",Toast.LENGTH_SHORT).show();
                return;
            }
            Toast.makeText(getContext(),"success",Toast.LENGTH_SHORT).show();
            showImage(processedImage);
        }
    }

}

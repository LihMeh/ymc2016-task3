package com.myarchway.mikhail.sign;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


public class SignPagerAdapter extends FragmentStatePagerAdapter {

    private Cursor cursor;

    private static final String[] querySelections = {MediaStore.Images.Media._ID,MediaStore.Images.Media.TITLE};
    // чтобы не использовать cursor.getColumnId, сами записываем их сюда.
    private static final int COLUMN_ID = 0;
    private static final int COLUMN_TITLE = 1;

    public SignPagerAdapter(FragmentManager fm, ContentResolver contentResolver) {
        super(fm);
        cursor = MediaStore.Images.Media.query(contentResolver,MediaStore.Images.Media.EXTERNAL_CONTENT_URI,querySelections);
    }

    @Override
    public Fragment getItem(int position) {
        cursor.moveToPosition(position);
        return PictureFragment.newInstance(cursor.getLong(COLUMN_ID));
    }

    @Override
    public int getCount() {
        return cursor.getCount();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position<0) return null;
        if (position>=getCount()) return null;
        cursor.moveToPosition(position);
        return cursor.getString(COLUMN_TITLE);
    }
}

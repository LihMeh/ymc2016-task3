package com.myarchway.mikhail.sign;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


import org.opencv.calib3d.Calib3d;
import org.opencv.core.*;
import org.opencv.android.Utils;
import org.opencv.imgproc.Imgproc;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.Features2d;

import java.util.ArrayList;
import java.util.List;


public class ImageProcessor {

    public static class SearchJob {
        public int patternResource;
        public int contourResource;
        public double contourMaxDist;
        public int forwardMatchesPercent;
        public int reverseMatchesPercent;
    }

    public enum MatchFilterType {
        NO_FILTER,
        MIN_DIST_MUL_N,
        GET_P_BEST
    }

    private List<SearchJob> searchJobs;

    private void fillJobs() {
        searchJobs = new ArrayList<SearchJob>();


        //переезд
        SearchJob trainJob = new SearchJob();
        trainJob.contourResource = R.drawable.contour_triangle;
        trainJob.patternResource = R.drawable.z12;
        trainJob.contourMaxDist = 0.1;
        trainJob.forwardMatchesPercent = 40;
        trainJob.reverseMatchesPercent = 20;
        searchJobs.add(trainJob);

        // Кирпич
        SearchJob brickJob = new SearchJob();
        brickJob.patternResource = R.drawable.z31;
        brickJob.contourResource = R.drawable.contour_round;
        brickJob.contourMaxDist = 0.05;
        brickJob.forwardMatchesPercent = 30;
        brickJob.reverseMatchesPercent = 30;
        searchJobs.add(brickJob);

        // движение запрещено
        SearchJob roundJob = new SearchJob();
        roundJob.patternResource = R.drawable.z32;
        roundJob.contourResource = R.drawable.contour_round;
        roundJob.contourMaxDist = 0.05;
        roundJob.forwardMatchesPercent = 30;
        roundJob.reverseMatchesPercent = 30;
        searchJobs.add(roundJob);

        // одностороннее
        SearchJob onewayJob = new SearchJob();
        onewayJob.patternResource = R.drawable.z55;
        onewayJob.contourResource = R.drawable.contour_oneway;
        onewayJob.contourMaxDist = 0.15;
        onewayJob.forwardMatchesPercent = 40;
        onewayJob.reverseMatchesPercent = 30;
        searchJobs.add(onewayJob);

        // переход 1
        SearchJob ppl1Job = new SearchJob();
        ppl1Job.patternResource = R.drawable.z5191;
        ppl1Job.contourResource = R.drawable.contour_square;
        ppl1Job.contourMaxDist = 0.05;
        ppl1Job.forwardMatchesPercent = 50;
        ppl1Job.reverseMatchesPercent = 50;
        searchJobs.add(ppl1Job);

        // переход 2
        SearchJob ppl2Job = new SearchJob();
        ppl2Job.patternResource = R.drawable.z5192;
        ppl2Job.contourResource = R.drawable.contour_square;
        ppl2Job.contourMaxDist = 0.05;
        ppl2Job.forwardMatchesPercent = 50;
        ppl2Job.reverseMatchesPercent = 50;
        searchJobs.add(ppl2Job);


    }

    private static final int FEATURE_DETECTOR_TYPE = FeatureDetector.ORB;
    private static final int DESCRIPTOR_EXTRACTOR_TYPE = DescriptorExtractor.ORB;
    private static final int DESCRIPTOR_MATCHER_TYPE = DescriptorMatcher.BRUTEFORCE_HAMMING;
    private static final int SUBSCENE_MIN_HEIGHT = 30;
    private static final int SUBSCENE_MIN_WIDTH = 30;
    private static final int SUBSCENE_MIN_FEATURES = 10;
    private static final double DISTANCE_FILTER = 70;

    private static final boolean PROCESS_ONLY_CONTOURS = true;
    private static final boolean PROCESS_ONLY_MATCHES = false;
    private static final boolean DEBUG_NUMBERS = false;


    FeatureDetector featureDetector;
    DescriptorExtractor descriptorExtractor;
    DescriptorMatcher descriptorMatcher;

    private Resources resources;

    public ImageProcessor(Resources resources) {
        this.resources = resources;
        fillJobs();
        featureDetector = FeatureDetector.create(FEATURE_DETECTOR_TYPE);
        descriptorExtractor = DescriptorExtractor.create(DESCRIPTOR_EXTRACTOR_TYPE);
        descriptorMatcher = DescriptorMatcher.create(DESCRIPTOR_MATCHER_TYPE);
    }

    private Mat bitmapToGrayMat(Bitmap bitmap) {
        Mat rgba = new Mat();
        Utils.bitmapToMat(bitmap,rgba);
        Mat gray = new Mat();
        Imgproc.cvtColor(rgba,gray,Imgproc.COLOR_RGBA2GRAY);
        rgba.release();
//        Mat normalized = new Mat();  // why reallocate? :)
//        Core.normalize(gray,normalized);
//        gray.release();
//        return normalized;
        return gray;
    }

    private Mat doCanny(Mat input) {
        Mat out = new Mat();
        Imgproc.Canny(input,out,500,100,3,true);
        return out;
    }



    private Bitmap matToBitmap(Mat image) {
        Bitmap bitmap = Bitmap.createBitmap(image.width(),image.height(),Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(image, bitmap);
        return bitmap;
    }


    private Mat getGrayMatFromResource(int resourceId) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeResource(resources, resourceId ,opts);
        Mat mat = bitmapToGrayMat(bitmap);
        bitmap.recycle();
        return mat;
    }

    public Bitmap findSigns(Bitmap src) {

        // Читаю картинку сцены -> sceneCanny
        Mat sceneGray = bitmapToGrayMat(src);
        Mat sceneCanny = doCanny(sceneGray);

        // Выделяю контуры на сцене
        List<MatOfPoint> sceneContours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(sceneCanny, sceneContours, hierarchy, Imgproc.CV_RETR_LIST, Imgproc.CV_CHAIN_APPROX_SIMPLE);

        Mat outImage = new Mat();
        Utils.bitmapToMat(src, outImage);
        Scalar drawColor = new Scalar(0, 255, 0);

        Point testStart = new Point(20, 20);

        for (SearchJob searchJob: searchJobs) {

            // Выедляю контур знака -> patternContour
            List<MatOfPoint> patternContours = new ArrayList<MatOfPoint>();
            Mat patternContourMat = getGrayMatFromResource(searchJob.contourResource);
            Mat patternHierarchy = new Mat();
            Imgproc.findContours(patternContourMat, patternContours, patternHierarchy, Imgproc.CV_RETR_EXTERNAL, Imgproc.CV_CHAIN_APPROX_SIMPLE);
            MatOfPoint patternContour = patternContours.get(0);
            patternContours.clear();
            patternContourMat.release();

            // Выделяю features и descriptors шаблона
            Mat patternMat = getGrayMatFromResource(searchJob.patternResource);
            MatOfKeyPoint patternKeyPoints = new MatOfKeyPoint();
            featureDetector.detect(patternMat, patternKeyPoints);
            Mat patternDescriptors = new Mat();
            descriptorExtractor.compute(patternMat, patternKeyPoints, patternDescriptors);
            patternMat.release();



            // Обходим контуры и находим похожие на наш знак. Собираем их в список
            for (int contourIdx = 0; contourIdx < sceneContours.size(); contourIdx++) {
                MatOfPoint sceneContour = sceneContours.get(contourIdx);
                double dist = Imgproc.matchShapes(patternContour, sceneContour, Imgproc.CV_CONTOURS_MATCH_I3, 0);
                if (dist > searchJob.contourMaxDist) continue;

                // Если мы дошли до сюда, значит нам следует проверить, не принадлежит ли контур нужному знаку

                // Выделяем часть сцены, в которой ищем знак -> subScene
                Rect sceneRect = Imgproc.boundingRect(sceneContour);

                if ((sceneRect.height < SUBSCENE_MIN_HEIGHT) || (sceneRect.width < SUBSCENE_MIN_WIDTH))
                    continue; // Слишком маленькие фрагменты нас не интересуют


                Mat subScene = sceneGray.submat(sceneRect);

                if (PROCESS_ONLY_CONTOURS) {                        // for debug

                    //subScene.copyTo(outImage.submat(sceneRect));
                    Imgproc.drawContours(outImage, sceneContours, contourIdx, drawColor, 3);
                    continue;
                }


                // выделяем features и descriptors
                MatOfKeyPoint subSceneKeypoints = new MatOfKeyPoint();
                featureDetector.detect(subScene, subSceneKeypoints);
                if (subSceneKeypoints.rows() < SUBSCENE_MIN_FEATURES) continue;
                Mat subSceneDescriptors = new Mat();
                descriptorExtractor.compute(subScene, subSceneKeypoints, subSceneDescriptors);


                // Сравниваем. СОответстует ли картинка внутки контура нашему шаблону
                MatOfDMatch matches = new MatOfDMatch();
                descriptorMatcher.match(patternDescriptors, subSceneDescriptors, matches);

                if (PROCESS_ONLY_MATCHES) {
                    subScene.copyTo(outImage.submat(sceneRect));
                    List<KeyPoint> lst = subSceneKeypoints.toList();
                    for (DMatch match : matches.toList()) {
                        Imgproc.drawMarker(outImage.submat(sceneRect), lst.get(match.trainIdx).pt, drawColor);
                    }
                    continue;
                }


                // Сколько совпадений/
                int goodMatchesCount = 0;
                for (DMatch match : matches.toList()) {
                    if (match.distance < DISTANCE_FILTER) {
                        goodMatchesCount++;
                    }
                }


                int forwardMatchesToExpect = (patternKeyPoints.rows()) * searchJob.forwardMatchesPercent / 100;
                int reverseMatchesToExpect = (subSceneKeypoints.rows()) * searchJob.reverseMatchesPercent / 100;

                if (DEBUG_NUMBERS) {
                    String strToPut = String.valueOf(goodMatchesCount) + " / " + String.valueOf(patternKeyPoints.rows()) + " /" + String.valueOf(subSceneKeypoints.rows());
                    Imgproc.putText(outImage, strToPut, testStart, Core.FONT_HERSHEY_PLAIN, 2.0, drawColor);
                    testStart.y += 20;
                }


                if (!((goodMatchesCount >= forwardMatchesToExpect) && (goodMatchesCount >= reverseMatchesToExpect)))
                    continue;



                Imgproc.rectangle(outImage, sceneRect.br(), sceneRect.tl(), drawColor, 3);


            }

        }

        sceneCanny.release();
        sceneGray.release();

        Bitmap out = matToBitmap(outImage);
        outImage.release();
        return out;
    }



}
